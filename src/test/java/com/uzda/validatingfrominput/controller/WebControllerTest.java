package com.uzda.validatingfrominput.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

@SpringBootTest
@AutoConfigureMockMvc
public class WebControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void checkPersonNameMissingThenFailure() throws Exception {

        MockHttpServletRequestBuilder createPerson = post("/")
                .param("age", "25");

        mockMvc.perform(createPerson)
                .andExpect(model().hasErrors());

    }

    @Test
    public void checkPersonNameTooShortThenFailure() throws Exception {

        MockHttpServletRequestBuilder createPerson = post("/")
                .param("name", "V")
                .param("age", "18");

        mockMvc.perform(createPerson)
                .andExpect(model().hasErrors());

    }

    @Test
    public void checkPersonAgeMissingThenFailure() throws Exception {

        MockHttpServletRequestBuilder createPerson = post("/")
                .param("name", "Lukasz");

        mockMvc.perform(createPerson)
                .andExpect(model().hasErrors());

    }

    @Test
    public void checkPersonAgeTooYoungThenFailure() throws Exception {

        MockHttpServletRequestBuilder createPerson = post("/")
                .param("name", "Ali")
                .param("age", "3");

        mockMvc.perform(createPerson)
                .andExpect(model().hasErrors());

    }

    @Test
    public void checkPersonValidInputThenSuccess() throws Exception {

        MockHttpServletRequestBuilder createPerson = post("/")
                .param("age", "22")
                .param("name", "Bob");

        mockMvc.perform(createPerson)
                .andExpect(model().hasNoErrors());

    }


}